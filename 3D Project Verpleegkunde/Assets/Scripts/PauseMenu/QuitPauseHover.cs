﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using TMPro;

public class QuitPauseHover : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler
{
    [SerializeField] private GameObject QuitPauseImage;

    public TextMeshProUGUI quitText;
    public void OnPointerEnter(PointerEventData eventData)
    {
        //Debug.Log("Hovered");
        QuitPauseImage.SetActive(true);
        quitText.color = Color.white;
    }
    public void OnPointerExit(PointerEventData eventData)
    {
        QuitPauseImage.SetActive(false);
        quitText.color = Color.grey;

    }

    public void OnPointerDown(PointerEventData eventData)
    {
        QuitPauseImage.SetActive(false);
        quitText.color = Color.grey;

    }
}
