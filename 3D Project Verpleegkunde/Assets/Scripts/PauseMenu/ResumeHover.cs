﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using TMPro;

public class ResumeHover : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler
{
    [SerializeField] private GameObject resumeImage;

    public TextMeshProUGUI resumeText;

    public void OnPointerEnter(PointerEventData eventData)
    {
        //Debug.Log("Hovered");
        resumeImage.SetActive(true);
        resumeText.color = Color.white;
    }
    public void OnPointerExit(PointerEventData eventData)
    {
        resumeImage.SetActive(false);
        resumeText.color = Color.grey;

    }

    public void OnPointerDown(PointerEventData eventData)
    {
        resumeImage.SetActive(false);
        resumeText.color = Color.grey;

    }
}