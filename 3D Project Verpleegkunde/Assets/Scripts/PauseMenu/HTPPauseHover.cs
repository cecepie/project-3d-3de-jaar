﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using TMPro;

public class HTPPauseHover : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler
{
    [SerializeField] private GameObject HTPPauseImage;

    public TextMeshProUGUI HTPPauseText;
    public void OnPointerEnter(PointerEventData eventData)
    {
        //Debug.Log("Hovered");
        HTPPauseImage.SetActive(true);
        HTPPauseText.color = Color.white;

    }
    public void OnPointerExit(PointerEventData eventData)
    {
        HTPPauseImage.SetActive(false);
        HTPPauseText.color = Color.grey;

    }

    public void OnPointerDown(PointerEventData eventData)
    {
        HTPPauseImage.SetActive(false);
        HTPPauseText.color = Color.grey;

    }
}