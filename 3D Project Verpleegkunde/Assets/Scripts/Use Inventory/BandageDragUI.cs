﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class BandageDragUI : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IEndDragHandler, IDragHandler
{
    private CanvasGroup canvasGroup;
    public static GameObject bandageObject = null;
    public static GameObject tempObject = null;

    public static bool hasBandagePatient2 = false;
    public static bool hasBandagePatient4 = false;

    private void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
    }
    public void OnBeginDrag(PointerEventData eventData)
    {
        canvasGroup.blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = Input.mousePosition;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        canvasGroup.blocksRaycasts = true;
        //put bandage back in original position
        transform.localPosition = new Vector3(0, 108, 0);
        foreach (GameObject hover in eventData.hovered)
        {
            if (hover.name == "BandageArmPatient2")
            {
                Debug.Log(hover);
                //activate bandage model
                PlayerInteract.patient.transform.Find("BandageArm").gameObject.SetActive(true);
                hasBandagePatient2 = true;
                break;
            }
            if (hover.name == "BandageArmPatient4")
            {
                Debug.Log(hover);
                //activate bandage model
                PlayerInteract.patient.transform.Find("Armature/mixamorig:Hips/mixamorig:Spine/mixamorig:Spine1/mixamorig:Spine2/mixamorig:RightShoulder/mixamorig:RightArm/mixamorig:RightForeArm/BandageArm").gameObject.SetActive(true);
                hasBandagePatient4 = true;
                break;
            }
            //name every drop image different to find different bandage models
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {

    }
}
