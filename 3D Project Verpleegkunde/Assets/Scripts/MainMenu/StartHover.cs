﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using TMPro;

public class StartHover : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler
{
    [SerializeField] private GameObject startImage;

    public TextMeshProUGUI startText;

    public void OnPointerEnter(PointerEventData eventData)
    {
        //Debug.Log("Hovered");
        startImage.SetActive(true);
        startText.color = Color.white;
    }
    public void OnPointerExit(PointerEventData eventData)
    {
        startImage.SetActive(false);
        startText.color = Color.grey;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        startImage.SetActive(false);
        startText.color = Color.grey;
    }
}
