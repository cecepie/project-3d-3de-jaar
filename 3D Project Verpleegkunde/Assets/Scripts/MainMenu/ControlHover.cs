﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using TMPro;

public class ControlHover : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler
{
    [SerializeField] private GameObject controlsImage;

    public TextMeshProUGUI controlsText;


    public void OnPointerEnter(PointerEventData eventData)
    {
        //Debug.Log("Hovered");
        controlsImage.SetActive(true);
        controlsText.color = Color.white;
    }
    public void OnPointerExit(PointerEventData evenData)
    {
        controlsImage.SetActive(false);
        controlsText.color = Color.grey;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        controlsImage.SetActive(false);
        controlsText.color = Color.grey;

    }
}

