﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Pills", menuName = "Inventory System/Items/Pills")]
public class PillsObject : ItemObject
{
    public int painReduction;
    public void Awake()
    {
        type = ItemType.Pills;
    }
}
