﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Feedback : MonoBehaviour
{
        [SerializeField] private GameObject success;
        [SerializeField] private GameObject failed;

        void Awake() 
        {
            success.SetActive(false);
            failed.SetActive(true);
        }
        
        void Update() 
        {
            if (BandageDragUI.hasBandagePatient2 == true)
            {
                success.SetActive(true);
                failed.SetActive(false);                
            }
            else
            {
                success.SetActive(true);
                failed.SetActive(true);
            }
            
        }
        
}
