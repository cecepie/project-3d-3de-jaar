﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.UI;

public class PatientInteraction : MonoBehaviour
{
    [SerializeField] private GameObject medicationOptions;
    [SerializeField] private GameObject patientInteractionMenu;
    [SerializeField] private GameObject radialMenu;
    [SerializeField] private GameObject[] patientInteractionMenuItemsToHide;

    private string pulseOption;
    private string breathingOption;
    private string consciousOption;
    private string nameOption;
    private GameObject[] patientInfo;
    public Transform[] children;

    public void TriageButton()
    {
        foreach (GameObject item in patientInteractionMenuItemsToHide)
        {
            item.SetActive(false);
        }
        radialMenu.SetActive(true);
    }

    public void TriageBackbutton()
    {
        radialMenu.SetActive(false);
        foreach (GameObject item in patientInteractionMenuItemsToHide)
        {
            item.SetActive(true);
        }
    }

    public void GeneralInfoPatient()
    {
        patientInfo = PlayerInteract.patientInfoPrefabArray;

        foreach (GameObject item in patientInfo)
        {
            if (item.activeInHierarchy)
            {
                children = new Transform[item.transform.childCount];
                for (int i = 0; i < item.transform.childCount; i++)
                {
                    children[i] = item.transform.GetChild(i);
                }
            }
        }

        foreach (Transform item in children)
        {
            if (item.tag == "Name Option")
            {
                nameOption = PlayerInteract.patient.patientInfo.patientName.ToString();

                item.GetComponent<TextMeshProUGUI>().text = nameOption;
            }

            if (item.tag == "Patient Photo")
            { 
                item.GetComponent<Image>().sprite = PlayerInteract.patient.patientInfo.photoOfPatient;
            }
        }
    }

    public void ConsciousButton()
    {
        //set conscious text value on clipboard to conscious value from scriptable object
        patientInfo = PlayerInteract.patientInfoPrefabArray;

        foreach (GameObject item in patientInfo)
        {
            if (item.activeInHierarchy)
            {
                children = new Transform[item.transform.childCount];
                for (int i = 0; i < item.transform.childCount; i++)
                {
                    children[i] = item.transform.GetChild(i);
                }
            }
        }

        foreach (Transform item in children)
        {
            if (item.tag == "Conscious Option")
            {
                consciousOption = PlayerInteract.patient.patientInfo.conscious.ToString();

                item.GetComponent<TextMeshProUGUI>().text = consciousOption;
            }
        }
    }

    public void PulseButton()
    {
        //set pulse text value on clipboard to pulse value from scriptable object
        patientInfo = PlayerInteract.patientInfoPrefabArray;

        foreach (GameObject item in patientInfo)
        {
            if (item.activeInHierarchy)
            {
                children = new Transform[item.transform.childCount];
                for (int i = 0; i < item.transform.childCount; i++)
                {
                    children[i] = item.transform.GetChild(i);
                }
            }
        }

        foreach (Transform item in children)
        {
            if (item.tag == "Pulse Option")
            {
                pulseOption = PlayerInteract.patient.patientInfo.pulse.ToString();

                item.GetComponent<TextMeshProUGUI>().text = pulseOption;
            }
        }
    }

    public void BreathingButton()
    {
        //set breathing text value on clipboard to breathing value from scriptable object 
        patientInfo = PlayerInteract.patientInfoPrefabArray;

        foreach (GameObject item in patientInfo)
        {
            if (item.activeInHierarchy)
            {
                children = new Transform[item.transform.childCount];
                for (int i = 0; i < item.transform.childCount; i++)
                {
                    children[i] = item.transform.GetChild(i);
                }
            }
        }

        foreach (Transform item in children)
        {
            if (item.tag == "Breathing Option")
            {
                breathingOption = PlayerInteract.patient.patientInfo.breathing.ToString();

                item.GetComponent<TextMeshProUGUI>().text = breathingOption;
            }
        }
    }

    public void PainButton()
    {
        if (PlayerInteract.patient.patientInfo.conscious != Conscious.Unresponsive)
        {
            medicationOptions.SetActive(true);
        }
        else
        {
            Debug.Log("patient is unconscious and can't react");
        }
    }
    public void BackButton()
    {
        medicationOptions.SetActive(false);
        patientInteractionMenu.SetActive(true);
    }
}
