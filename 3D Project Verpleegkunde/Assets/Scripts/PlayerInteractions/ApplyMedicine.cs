﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplyMedicine : MonoBehaviour
{
    [SerializeField] private InventoryObject inventoryObject;
    private SyringeObject syringeObject;

    void Start()
    {
        syringeObject = new SyringeObject();
    }

    public void ApplySyringe()
    {
        //remove 1 syringe item from inventory
        //reduce pain with reduce amount from scriptable object
        inventoryObject.RemoveItem(ItemType.Syringe, 1);
        Debug.Log("Pain reduced by " + syringeObject.painReduction);
    }
    public void ApplyBandage()
    {
        //remove 1 bandage item from inventory
        //stop bleeding
        inventoryObject.RemoveItem(ItemType.Bandage, 1);
    }
    public void ApplyPills()
    {
        //remove 1 pills item from inventory
        //reduce pain with reduce amount from scriptable object
        inventoryObject.RemoveItem(ItemType.Pills, 1);
    }

}
